## 2018-11-14 Release 2.6.2
  - Fixed a bug where if only 1 collection/organization was used, the front page stats displayed
    "organization" instead of "collection".